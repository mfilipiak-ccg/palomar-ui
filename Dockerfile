FROM python:3-alpine

RUN pip install flask requests requests-opentracing pytest

COPY app.py /app/app.py
COPY templates /app/templates

ENV FLASK_APP=app.py
WORKDIR /app

CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]
