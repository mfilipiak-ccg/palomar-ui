""" Test the integration for ui project
"""
import os
import pytest
import requests

UI_HOSTNAME = os.environ.get('UI_HOSTNAME', 'palomar-ui')


@pytest.mark.integration
def test_ui():
    """ Make sure that ui portfolio queries return results. Note, that
        this exercises the ticker service as an external dependency.
    """
    response = requests.get(f"http://{UI_HOSTNAME}/user1/portfolio/balance")
    assert response.status_code == 200
