""" UI facing APIs """
import os
import requests

from flask import Flask, jsonify, render_template, request


class SessionTracing(requests.sessions.Session): # pylint: disable=too-few-public-methods
    """ Hooking in requests to provide tracing """

    def __init__(self, tracer=None, propagate=True, span_tags=None): # pylint: disable=unused-argument
        super(SessionTracing, self).__init__()

    def request(self, method, url, *args, **kwargs):
        """ Hooks the request to add tracing """
        headers = kwargs.setdefault('headers', {})
        for key, val in request.headers.items():
            low_key = key.lower()
            if low_key.startswith('x-b3'):
                headers[key] = val
        return super(SessionTracing, self).request(method, url, *args,
                                                   **kwargs)


APP = Flask(__name__)

PORTFOLIO_HOSTNAME = os.environ.get('PORTFOLIO_HOSTNAME', 'portfolio.palomar')

@APP.route('/')
def index_page():
    """ Provides the UI """
    return render_template('index.html')


@APP.route('/<user_id>/portfolio/balance', methods=['GET'])
def portfolio_balance(user_id):
    """ Gets a users balance """
    url = f"http://{PORTFOLIO_HOSTNAME}/{user_id}/balance"

    response = SessionTracing().get(url, timeout=3)
    if response.status_code != 200:
        print('Error querying portfolio service', response.reason)
        raise Exception("Error retrieving portfolio balance")

    balance_info = response.json()

    return jsonify({
        'user_id': user_id,
        'epoch_time': balance_info['epoch_time'],
        'balance': balance_info['balance']
    })


@APP.route('/health')
def health_check():
    """ API for health check """
    return jsonify({'healthy': True})
